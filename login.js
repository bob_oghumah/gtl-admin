import { Selector } from 'testcafe';

fixture `Getting Started`
    .page `https://dev-tandztech-gotoloans.tekstackapps.com/admin`;

test('My first test', async t => {
    await t
        .typeText('#username-input', 'lenderadmin@tekstack.ca')
        .typeText('#password-input', 'AdminGTL*$2')
        .click('#signin-button');
});